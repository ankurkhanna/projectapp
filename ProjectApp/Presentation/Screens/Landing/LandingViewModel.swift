//
//  LandingViewModel.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class LandingViewModel: ViewModel {
    private let _output: ReplaySubject<OutputEvent> = ReplaySubject<OutputEvent>.create(bufferSize: 1)
    var output: OutputSignal<OutputEvent> {
        return _output.asObservable()
    }
    
    var input: InputSignal<InputEvent> = PublishSubject<InputEvent>()
     
    private let musicRepository: MusicRepository
    private let _sections: BehaviorRelay<[EditorialListingSection]> = BehaviorRelay(value: [])
     
    init(musicRepository: MusicRepository) {
        self.musicRepository = musicRepository
        super.init()
    }

  
    func set(viewEvent: LandingViewModel.ViewEvent) -> Disposable {
        let compositeDisposable: CompositeDisposable = CompositeDisposable()
        
        let startScreenDisposable = viewEvent.startScreen
            .flatMapLatest { [weak self] (_) -> Observable<[EditorialListingSection]> in
                guard let strongSelf = self else { return Observable.of([]) }
                return strongSelf.loadData()
            }
            .asDriver(onErrorJustReturn: [])
            .drive(_sections)
        
        _ = compositeDisposable.insert(startScreenDisposable)
        return compositeDisposable
    }

    func generateViewData() -> LandingViewModel.ViewData {
        return ViewData(sections: _sections.asDriver())
    }
    
    private func loadData() -> Observable<[EditorialListingSection]> {
        return self.musicRepository.getEditoials().trackActivity(activityIndicator)
            .map({(editorial: Editorial) -> [EditorailItem] in
                return editorial.editorialItems
            })
            .map({ (editorailItems: [EditorailItem]) -> [EditorialListingSection] in
                var items: [EditorialListingSectionItem] = []
                for editorial in editorailItems {
                    items.append(.editorialItem(editorial: editorial))
                }
                
                var section: [EditorialListingSection] = []
                section.append(.editorialListingSection(items: items))
                return section
            })
    }
}

extension LandingViewModel {
    enum InputEvent: ViewModelInput {
    }
    enum OutputEvent: ViewModelOutput {

    }
    struct ViewEvent: ViewModelViewEvent {
        let startScreen: Observable<Void>
    }
    
    struct ViewData: ViewModelViewData {
        let sections: Driver<[EditorialListingSection]>
    }
}


//MARK: RxDataSource Table Items
enum EditorialListingSection {
    case editorialListingSection(items: [EditorialListingSectionItem])
}

enum EditorialListingSectionItem {
    case editorialItem(editorial: EditorailItem)
}

extension EditorialListingSection: SectionModelType {

    typealias Item = EditorialListingSectionItem

    var items: [EditorialListingSectionItem] {
        switch self {
        case .editorialListingSection(items: let items):
            return items
        }
    }

    init(original: EditorialListingSection, items: [Item]) {
        switch original {
        case .editorialListingSection:
            self = .editorialListingSection(items: items)
        }
    }
}

