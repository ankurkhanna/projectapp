//
//  LandingBuilder.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import UIKit

struct LandingBuilder: Builder {
  
    typealias ObjectType = LandingViewController
    struct BuilderConfiguration {}
    
    static func build(_ builder: LandingBuilder.BuilderConfiguration) -> ObjectType {
        let vc: LandingViewController = UIViewController.instantiateViewController(ofType: LandingViewController.self)
        let musicRepository: MusicRepository = Dependancy.container.resolve(MusicRepository.self)!
        let vm: LandingViewModel = LandingViewModel(musicRepository: musicRepository)
        vc.viewModel = vm
        return vc
    }
}
