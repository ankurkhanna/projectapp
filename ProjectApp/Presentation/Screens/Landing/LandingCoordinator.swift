//
//  LandingCoordinator.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class LandingCoordinator: Coordinator<LandingViewModel.OutputEvent> {
    static var coordinatorID: String = "landing"
    
    private let _releaseSignal: PublishSubject<Void> = PublishSubject<Void>()
    var releaseSignal: Observable<Void> {
        return _releaseSignal.asObservable()
    }
    
    override func start() -> CoordinatorResult<LandingViewModel.OutputEvent> {
        //Build ViewController
        let vc: LandingViewController = LandingBuilder.build(LandingBuilder.BuilderConfiguration())
        
        //Bind Output Event
        vc.viewModel.output
            .subscribe(onNext: { (outputEvent: LandingViewModel.OutputEvent) in
            })
            .disposed(by: disposeBag)
        
        navigationController.pushViewController(vc, animated: true)
        return CoordinatorResult(viewModelResult: Observable.empty(), releaseSignal: releaseSignal)
    }
}
