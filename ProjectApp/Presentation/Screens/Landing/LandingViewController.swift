//
//  LandingViewController.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class LandingViewController: BaseViewController, ViewControllerProtocol {

    private let veStartScreen: PublishSubject<Void> = PublishSubject<Void>()
    @IBOutlet private var tableView: UITableView!
    
    var viewModel: LandingViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        veStartScreen.onNext(())
    }
    
    override func setupUI() {
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        let editorialTableViewCell = UINib(nibName: EditorialTableViewCell.subjectLabel, bundle: nil)
        tableView.register(editorialTableViewCell, forCellReuseIdentifier: EditorialTableViewCell.subjectLabel)
        self.navigationController?.navigationBar.topItem?.title = "Editoral"
    }
    
    override func setViewModelViewEvent() {
        let viewModelEvent = LandingViewModel.ViewEvent(startScreen: veStartScreen.asObservable())
        viewModel.set(viewEvent: viewModelEvent).disposed(by: disposeBag)
    }
    
    override func bindViewData() {
     let viewData = viewModel.generateViewData()
        
        viewData.sections
            .drive(tableView.rx.items(dataSource: dataSource()))
            .disposed(by: disposeBag)
        
        viewModel.activityIndicator
            .drive(view.rx.isActivityIndicatorAnimating)
            .disposed(by: disposeBag)
    }
}

extension LandingViewController {

    func dataSource() -> RxTableViewSectionedReloadDataSource<EditorialListingSection> {
        let dataSource = RxTableViewSectionedReloadDataSource<EditorialListingSection> { (dataSource, tableView, indexPath, _) -> UITableViewCell in
            
            let cell = tableView.dequeueReusableCell(ofType: EditorialTableViewCell.self, at: indexPath)
            cell.selectionStyle = .none

            switch dataSource[indexPath] {
            case .editorialItem(editorial: let editorial):
                cell.configureContent(editorialItem: editorial)
            }
            return cell
        }
        return dataSource
    }
}

