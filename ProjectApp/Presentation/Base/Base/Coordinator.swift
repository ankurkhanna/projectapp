//
//  Coordinator.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

struct CoordinatorResult<T> {
    let viewModelResult: Observable<T>
    let releaseSignal: Observable<Void>
}

protocol CoordinatorAbstract {
    func coordinate<T>(to coordinator: BaseCoordinator<T>) -> CoordinatorResult<T>
}

typealias Coordinator<T> = BaseCoordinator<T>

// swiftlint:disable explicit_type_interface
/// Base abstract coordinator generic over the return type of the `start` method.
class BaseCoordinator<FlowCoordinatorResultsType>: NSObject, HasDisposeBag, Identifiable, CoordinatorAbstract {
    
    // HasDisposeBag.
    var disposeBag: DisposeBag = DisposeBag()
    let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    var id: String {
        return self.subjectLabel
    }
    
    /// Dictionary of the child coordinators. Every child coordinator should be added
    /// to that dictionary in order to keep it in memory.
    /// Key is an `identifier` of the child coordinator and value is the coordinator itself.
    /// Value type is `Any` because Swift doesn't allow to store generic types in the array.
    private var childCoordinators: [String: Any] = [String: Any]()
    
    /// Stores coordinator to the `childCoordinators` dictionary.
    ///
    /// - Parameter coordinator: Child coordinator to store.
    private func store<T>(coordinator: BaseCoordinator<T>) {
        childCoordinators[coordinator.id] = coordinator
    }
    
    /// Release coordinator from the `childCoordinators` dictionary.
    ///
    /// - Parameter coordinator: Coordinator to release.
    private func release<T>(coordinator: BaseCoordinator<T>) {
        childCoordinators[coordinator.id] = nil
    }
    
    /// 1. Stores coordinator in a dictionary of child coordinators.
    /// 2. Calls method `start()` on that coordinator.
    /// 3. On the `onNext:` of returning observable of method `start()` removes coordinator from the dictionary.
    ///
    /// - Parameter coordinator: Coordinator to start.
    /// - Returns: Result of `start()` method.
    func coordinate<T>(to coordinator: BaseCoordinator<T>) -> CoordinatorResult<T> {
        let coordinatorResult = coordinator.start()
        _ = coordinatorResult.releaseSignal
            .take(1)
            .subscribe(onNext: { [weak self] _ in
                self?.release(coordinator: coordinator)
            })
        
        store(coordinator: coordinator)
        return coordinatorResult
    }
    
    /// Starts job of the coordinator.
    ///
    /// - Returns: Result of coordinator job.
    func start() -> CoordinatorResult<FlowCoordinatorResultsType> {
        fatalError("Start method should be implemented.")
    }
}
