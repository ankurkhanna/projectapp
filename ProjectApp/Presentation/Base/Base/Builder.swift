//
//  Builder.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

protocol Builder {
    /// The type of the object to be built
    associatedtype ObjectType
    /// The type of the builder
    associatedtype BuilderConfiguration

    static func build(_ builder: BuilderConfiguration) -> ObjectType
}
