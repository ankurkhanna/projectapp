//
//  ViewModel.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

typealias ViewModel = BaseViewModel & ViewModelProtocol

protocol ViewModelProtocol: HasActivityIndicator, SubjectLabelable {
    associatedtype ViewEvent: ViewModelViewEvent
    associatedtype ViewData: ViewModelViewData
    associatedtype InputEvent: ViewModelInput
    associatedtype OutputEvent: ViewModelOutput
    
    func set(viewEvent: ViewEvent) -> Disposable
    func generateViewData() -> ViewData
    var output: OutputSignal<OutputEvent> { get }
    var input: InputSignal<InputEvent> { get }
}

protocol ViewModelViewEvent {
}

protocol ViewModelViewData {
}

protocol ViewModelOutput: Equatable {
}

protocol ViewModelInput: Equatable {
}

protocol HasActivityIndicator {
    var activityIndicator: ActivityIndicator { get }
}

typealias InputSignal<T> = PublishSubject<T>
typealias OutputSignal<T> = Observable<T>

class BaseViewModel: NSObject, HasActivityIndicator {
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    override init() {
        super.init()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
