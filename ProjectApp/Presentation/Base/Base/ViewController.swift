//
//  ViewController.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

typealias ViewController = BaseViewController & ViewControllerProtocol

protocol ViewControllerProtocol: HasDisposeBag, SubjectLabelable { 
    func setViewModelViewEvent()
    func bindViewData()
    func setupUI()
}

class BaseViewController: UIViewController {
    
    var disposeBag: DisposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setViewModelViewEvent()
        bindViewData()
    }

    func setViewModelViewEvent() {
        fatalError("setViewModelViewEvent method must be implemented !!!")
    }
    
    func bindViewData() {
        fatalError("bindViewData method must be implemented !!!")
    }
    
    func setupUI() {
        fatalError("bindViewData method must be implemented !!!")
    }
}
