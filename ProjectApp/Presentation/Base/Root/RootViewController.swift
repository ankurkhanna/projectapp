//
//  RootViewController.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//


import Foundation
import RxSwift
import UIKit

class RootViewController: UINavigationController, ViewControllerProtocol {
    var disposeBag: DisposeBag = DisposeBag()
    var viewModel: RootViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setViewModelViewEvent() {
        let viewModelEvent: RootViewModel.ViewEvent = RootViewModel.ViewEvent()
        viewModel.set(viewEvent: viewModelEvent)
            .disposed(by: disposeBag)
    }
    
    func bindViewData() {}
    func setupUI() {}
}



