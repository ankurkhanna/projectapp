//
//  RootCoordinator.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//


import Foundation
import UIKit
import RxSwift

class RootCoordinator: Coordinator<Void> {
    static var coordinatorID: String = "root"

    private let window: UIWindow
    
    init(window: UIWindow?) {
        guard let window = window else {
            fatalError("There should be a window always")
        }
        self.window = window
        super.init(navigationController:
            RootBuilder.build(RootBuilder.BuilderConfiguration()))
    }
    
    override func start() -> CoordinatorResult<Void> {
        // Navigate to ViewController
        self.window.rootViewController = self.navigationController
        self.window.makeKeyAndVisible()
        self.showLandingPage()
  
        return CoordinatorResult(viewModelResult: Observable.never(), releaseSignal: Observable.never())
    }
    
    private func showLandingPage() {
        guard let navigationController: UINavigationController = window.rootViewController as? UINavigationController else {
            fatalError("The root view controller must be a navigation controller!")
        }
        let coordinator: LandingCoordinator = LandingCoordinator(navigationController: navigationController)
        _ = coordinate(to: coordinator)
    }
}
