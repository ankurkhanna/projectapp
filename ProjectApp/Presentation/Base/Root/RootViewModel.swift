//
//  RootViewModel.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//


import Foundation
import RxSwift

class RootViewModel: ViewModel {
    private let _output: ReplaySubject<OutputEvent> = ReplaySubject<OutputEvent>.create(bufferSize: 1)
    var output: OutputSignal<OutputEvent> {
        return _output.asObservable()
    }
    
    var input: InputSignal<InputEvent> = PublishSubject<InputEvent>()
    
    func set(viewEvent: RootViewModel.ViewEvent) -> Disposable {
        let compositeDisposable: CompositeDisposable = CompositeDisposable()
        return compositeDisposable
    }
    
    func generateViewData() -> RootViewModel.ViewData {
        return ViewData()
    }
}

extension RootViewModel {
    enum InputEvent: ViewModelInput {}
    enum OutputEvent: ViewModelOutput {}
    struct ViewEvent: ViewModelViewEvent {}
    struct ViewData: ViewModelViewData {}
}
