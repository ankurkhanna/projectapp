//
//  RootBuilder.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//


import Foundation

struct RootBuilder: Builder {
    typealias ObjectType = RootViewController
    struct BuilderConfiguration {}
    
    static func build(_ builder: RootBuilder.BuilderConfiguration) -> ObjectType {
        let vc: RootViewController = RootViewController()
        let vm: RootViewModel = RootViewModel()
        vc.viewModel = vm
        return vc
    }
}
