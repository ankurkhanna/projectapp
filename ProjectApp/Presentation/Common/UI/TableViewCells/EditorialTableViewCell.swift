//
//  EditorialTableViewCell.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import UIKit
import Kingfisher

class EditorialTableViewCell: TableViewCell {

    @IBOutlet private var editorialImageView: UIImageView!
    @IBOutlet private var editorialNameLabel: UILabel!

    func configureContent(editorialItem: EditorailItem) {
        editorialNameLabel.text = editorialItem.name
        editorialImageView.setImage(urlString: editorialItem.picture)
    }
}
