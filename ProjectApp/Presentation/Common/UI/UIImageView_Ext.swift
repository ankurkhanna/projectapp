//
//  UIImageView_Ext.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(
        urlString: String?
        , placeholderImage: UIImage = UIImage(imageLiteralResourceName: "music_placeholder") //swiftlint:disable:this force_unwrapping
        , completion: ((UIImage?) -> Void)? = nil
        , failure: ((Error?) -> Void)? = nil
        ) {
        
        guard let urlString = urlString, !urlString.isEmpty, let imageUrl = URL(string: urlString) else {
            self.image = placeholderImage
            return
        }
        
        let imageResource: ImageResource = ImageResource(downloadURL: imageUrl)
        
        self.kf.setImage(with: imageResource, placeholder: placeholderImage, options: [.transition(.fade(0.8))]) { (result) in
            switch result {
            case .success(let value):
                completion?(value.image)
            case .failure(let error):
                failure?(error)
            }
        }
    }
}


