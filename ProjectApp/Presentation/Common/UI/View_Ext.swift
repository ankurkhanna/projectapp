//
//  View_Ext.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

// swiftlint:disable explicit_type_interface explicit_init
extension UITableView {
    func dequeueReusableCell<T>(ofType cellType: T.Type = T.self,
                                at indexPath: IndexPath) -> T where T: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: cellType.classIdentifier,
                                             for: indexPath) as? T else {
                                                return T.instantiateView(ofType: T.self)
        }
        return cell
    }
}

extension UICollectionView {
    func dequeueReusableCell<T>(ofType cellType: T.Type = T.self,
                                at indexPath: IndexPath) -> T where T: UICollectionViewCell {

        guard let cell = dequeueReusableCell(withReuseIdentifier: cellType.classIdentifier, for: indexPath) as? T else {
                                                return T.instantiateView(ofType: T.self)
        }
        return cell
    }
}

extension UIViewController {
    static func instantiateViewController<T>(ofType type: T.Type = T.self) -> T where T: UIViewController {
        let viewController = T.init(nibName: T.classIdentifier, bundle: Bundle(for: T.self))
        return viewController
    }
}

extension UIView {
    static func instantiateView<T>(ofType type: T.Type = T.self) -> T where T: UIView {
        let nib = UINib(nibName: T.classIdentifier, bundle: Bundle(for: T.self))
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? T else {
            fatalError("The root view in \(nib) nib must be of type \(self)")
        }
        return view
    }
}

extension Reactive where Base: UIView {
    /// Bindable sink for `enabled` property.
    public var isActivityIndicatorAnimating: Binder<Bool> {
        return Binder(self.base) { view, value in
            view.isActivityIndicatorAnimating = value
        }
    }
}

extension UIView {
    var activityIndicatorTag: Int { return 999_999 }

    var isActivityIndicatorAnimating: Bool {
        get {
            if let activityIndicator = self.subviews.filter({ $0.tag == self.activityIndicatorTag }).first as? UIActivityIndicatorView
                , activityIndicator.isAnimating {
                return true
            } else {
                return false
            }
        }
        set {
            let activityIndicatorExist = subviews
                .contains(where: { (view: UIView) -> Bool in
                    view.tag == activityIndicatorTag
                })

            guard newValue != activityIndicatorExist else {
                return
            }

            if newValue {
                let activityIndicator = { () -> UIActivityIndicatorView in
                    let loader = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                    loader.style = UIActivityIndicatorView.Style.gray
                    loader.translatesAutoresizingMaskIntoConstraints = false
                    loader.tag = self.activityIndicatorTag
                    loader.startAnimating()
                    return loader
                }()


                let constraints = [
                    NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
                    NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0),
                    NSLayoutConstraint(item: activityIndicator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40),
                    NSLayoutConstraint(item: activityIndicator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
                ]

                addSubview(activityIndicator)
                addConstraints(constraints)

            } else {

                // Here we find the `UIActivityIndicatorView` and remove it from the view
                guard let activityIndicator = self.subviews.filter({ $0.tag == self.activityIndicatorTag }).first as? UIActivityIndicatorView
                    else { return }
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        }
    }
}
