//
//  TableViewCell.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol TableViewCellType: HasDisposeBag, ReusableView {}

class TableViewCell: UITableViewCell, TableViewCellType {
    var disposeBag: DisposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeOnReuse()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
