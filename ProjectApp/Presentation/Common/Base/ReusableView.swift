//
//  ReusableView.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

protocol ReusableView: AnyObject {
    func disposeOnReuse()
}

#if os(iOS)
import class UIKit.UIView

extension ReusableView where Self: UIView {
    func disposeOnReuse() {
        
        if var hasDisposeBag = self as? HasDisposeBag {
            hasDisposeBag.disposeBag = DisposeBag()
        }
        
        for case let reusableView as ReusableView in subviews {
            reusableView.disposeOnReuse()
        }
    }
}

#endif
