//
//  DeezerMusicProvider.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

protocol DeezerMusicProvider: Provider {
    func getEditoials() -> Observable<Editorial>
}

class DefaultDeezerMusicProvider: DeezerMusicProvider {
    private let environment: Environment
    private let networkClient: NetworkClient
 
    init(environment: Environment,
         networkClient: NetworkClient) {
        self.environment = environment
        self.networkClient = networkClient
    }
    
    func getEditoials() -> Observable<Editorial> {
        let request = URLRequestBuilder(with: environment.middlewareConfig.environment.baseUrl, path: "/editorial")
            .build()
        
        let response: Observable<Editorial> = networkClient.request(urlRequest: request)
        return response.map { (editorial: Editorial) in
            return editorial
        }
    }
}
