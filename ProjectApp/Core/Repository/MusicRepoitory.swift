//
//  MusicRepoitory.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

protocol MusicRepository: Repository {
    func getEditoials() -> Observable<Editorial>
}

class DefaultMusicRepository: MusicRepository {
    
    private let deezerMusicProvider: DeezerMusicProvider
    
    init(deezerMusicProvider: DeezerMusicProvider) {
        self.deezerMusicProvider = deezerMusicProvider
    }
    
    func getEditoials() -> Observable<Editorial> {
        return deezerMusicProvider.getEditoials()
    }
}
