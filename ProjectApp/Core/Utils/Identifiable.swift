//
//  Identifiable.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

protocol Identity {
    static var classIdentifier: String { get }
}

extension Identity {
    static var classIdentifier: String {
        return String(describing: self)
    }
}

extension NSObject: Identity {}
