//
//  Logger.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

let logger = Logger(enabledLoggingOptions: [.debug, .apiRequest, .apiResponse])

enum LoggingOption {

    /// Use this mode to log api requests info.
    /// `apiCalls` mode logs will be written when running with Debug build config only.
    case apiRequest

    /// Use this mode to log api responses info.
    /// `apiResponse` mode logs will be written when running with Debug build config only.
    case apiResponse

    /// Use this mode to log api responses in JSON format.
    /// `apiResponse` mode logs will be written when running with Debug build config only.
    case apiResponseInJSON

    /// Use this mode to log newly added code. Remove it or change its logging mode once the code is deemed safe.
    /// `debug` mode logs will be written when running any build config and will be uploaded to online logging services.
    case debug

    /// Use this mode to log extra informations.
    /// `verbose` mode logs will be written when running Debug build.
    case verbose
}

final class Logger {
    let enabledLoggingOptions: [LoggingOption]
    init(enabledLoggingOptions: [LoggingOption]) {
        self.enabledLoggingOptions = enabledLoggingOptions
    }

    func log(title: String) {
        let output = "✅✅✅ Success: \(title) ✅✅✅\n"
        log(output, loggingOption: .debug)
    }

    func log(request: URLRequest, message: String? = nil) {
        var output = "############# API ###############"

        // Add debug message
        output += "\n\nRequesting:\n"
        if let message = message {
            output = "✅✅✅ \(output)\(message)\n"
        }

        // Add URL
        output += "URL: \n\(request.description)\n\n"
        if let httpBody = request.httpBody, let httpBodyString = String(data: httpBody, encoding: String.Encoding.utf8) {
            output += "httpBody: \n\(httpBodyString)\n\n"
        } else {
            output += "NO httpBody\n\n"
        }

        // Add headers
        if let headers = request.allHTTPHeaderFields {
            output += "Headers: \n\(headers)\n\n"
        } else {
            output += "NO headers\n\n"
        }

        output += "############# END OF API ###############"
        log(output, loggingOption: .apiRequest)
    }

//    func log<T>(response: DataResponse<T>) {
//        var output = "\n\n############ RESPONSE ###############\n\n"
//        output += "Request:\n"
//        if let request = response.request {
//            // Add URL
//            output += "URL: \n\(request.description)\n\n"
//
//            // Add HTTP method
//            if let method = request.httpMethod {
//                output += "Method: \(method)\n\n"
//            }
//
//            // Add HTTP Body
//            if let httpBody = request.httpBody, let httpBodyString = String(data: httpBody, encoding: String.Encoding.utf8) {
//                output += "httpBody: \n\(httpBodyString)\n\n"
//            } else {
//                output += "NO httpBody\n\n"
//            }
//
//            // Add headers
//            if let headers = request.allHTTPHeaderFields {
//                output += "Headers: \n\(headers)\n\n"
//            } else {
//                output += "NO headers\n\n"
//            }
//        } else {
//            output += "No request found\n\n"
//        }
//
//        output += "Received:\n"
//
//        if response.value != nil {
//            output += "Success:\n\n"
//            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                output += "\(utf8Text)\n\n"
//            }
//        }
//
//        if response.error != nil {
//            output += "Failure:\n\n"
//            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                output += "\(utf8Text)\n\n"
//            }
//        }
//
//        output += "Timeline of the complete lifecycle of the request:\n\n"
//        output += "\(response.timeline)"
//        output += "\n\n########### END OF RESPONSE #############\n\n"
//
//        log(output, loggingOption: .apiResponse)
//    }

    func log(responseInJSONwithData data: Data) {
        let output = "\n\n############## JSON Response #################\n\n"
        if let JSONString = String(data: data, encoding: String.Encoding.utf8) {
            log("\(output) \(JSONString) \n\n############# END OF JSON Response #################\n\n", loggingOption: .apiResponseInJSON)
        }
    }
    
    fileprivate func log(_ output: String, loggingOption: LoggingOption) {
        if enabledLoggingOptions.contains(loggingOption) {
            print(output)  //Can use a logger service to report eg. crashltyics
        }
    }
}
