//
//  HasDisposeBag.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

protocol HasDisposeBag {
    var disposeBag: DisposeBag { get set }
}
