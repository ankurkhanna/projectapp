//
//  RxOptionalChain.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

extension Optional {
    func `or`(_ value: Wrapped?) -> Optional {
        return self ?? value
    }
    func `or`(_ value: Wrapped) -> Wrapped {
        return self ?? value
    }
}
