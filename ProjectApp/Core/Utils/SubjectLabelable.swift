//
//  SubjectLabelable.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

protocol SubjectLabelable {
    static var subjectLabel: String { get }
    var subjectLabel: String { get }
}

extension SubjectLabelable {
    static var subjectLabel: String {
        let result: String = String(describing: self)
        return result
    }

    var subjectLabel: String {
        let result: String = String(describing: type(of: self))
        return result
    }
}

extension NSObject: SubjectLabelable {}
