//
//  ObservableType+Shortcuts.swift
//  RxShortcuts
//
//  Created by sunshinejr on 11/13/2016.
//  Copyright (c) 2016 sunshinejr. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
public extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy {
    /**
     Subscribes an element handler to an observable sequence.
     
     - parameter onNext: Action to invoke for each element in the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
    func driveNext(_ onNext: @escaping (Element) -> Void) -> Disposable {
        return drive(onNext: onNext)
    }
}

public extension ObservableType {

    /**
     Invokes an action for each Next event in the observable sequence, and propagates all observer messages through the result sequence.
     
     - parameter onNext: Action to invoke for each element in the observable sequence.
     - returns: The source sequence with the side-effecting behavior applied.
     */
     func doOnNext(_ onNext: @escaping (Element) throws -> Void) -> Observable<Element> {
        return `do`(onNext: onNext)
    }

    /**
     Invokes an action for the Error event in the observable sequence, and propagates all observer messages through the result sequence.
     
     - parameter onError: Action to invoke upon errored termination of the observable sequence.
     - returns: The source sequence with the side-effecting behavior applied.
     */
     func doOnError(_ onError: @escaping (Swift.Error) throws -> Void) -> Observable<Element> {
        return `do`(onError: onError)
    }

    /**
     Invokes an action for the Completed event in the observable sequence, and propagates all observer messages through the result sequence.
     
     - parameter onCompleted: Action to invoke upon graceful termination of the observable sequence.
     - returns: The source sequence with the side-effecting behavior applied.
     */
     func doOnCompleted(_ onCompleted: @escaping () throws -> Void) -> Observable<Element> {
        return `do`(onCompleted: onCompleted)
    }

    /**
     Subscribes an element handler to an observable sequence.
     
     - parameter onNext: Action to invoke for each element in the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
     func subscribeNext(_ onNext: @escaping (Element) -> Void) -> Disposable {
        return subscribe(onNext: onNext)
    }

    /**
     Subscribes an error handler to an observable sequence.
     
     - parameter onError: Action to invoke upon errored termination of the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
     func subscribeError(_ onError: @escaping (Swift.Error) -> Void) -> Disposable {
        return subscribe(onError: onError)
    }

    /**
     Subscribes a completion handler to an observable sequence.
     
     - parameter onCompleted: Action to invoke upon graceful termination of the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
     func subscribeCompleted(_ onCompleted: @escaping () -> Void) -> Disposable {
        return subscribe(onCompleted: onCompleted)
    }
}
