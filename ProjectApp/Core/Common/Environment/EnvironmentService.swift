//
//  EnvironmentService.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

protocol EnvironmentService {
    associatedtype ServiceType
    var staging: ServiceType { get }
    var production: ServiceType { get }

    func getEnvironment(type: EnvironmentType) -> ServiceType
}

extension EnvironmentService {
    func getEnvironment(type: EnvironmentType) -> ServiceType {
        switch type {
        case .production:
            return production
        case .staging:
            return staging
        }
    }
}
