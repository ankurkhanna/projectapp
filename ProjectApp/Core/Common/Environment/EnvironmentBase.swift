//
//  EnvironmentBase.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

struct EnvironmentBase {
    // Currently the base url remains the same, but we can have a different one in a real staging env.
    static let MiddlewareStagingBaseUrl: String = "https://api.deezer.com"
    static let MiddlewareProductionBaseUrl: String = "https://api.deezer.com"
}
