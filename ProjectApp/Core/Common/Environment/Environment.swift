//
//  Environment.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

enum EnvironmentType {
    case staging
    case production
}

protocol Environment {
    var type: EnvironmentType { get }
    var middlewareConfig: MiddlewareConfig { get }
}

struct StagingEnvironment: Environment {
    var type: EnvironmentType {
        return .staging
    }
}

struct ProductionEnvironment: Environment {
    var type: EnvironmentType {
        return .production
    }
}

extension Environment {
    var middlewareConfig: MiddlewareConfig {
        return DeezerEnvironmentService().getEnvironment(type: type)
    }
}
