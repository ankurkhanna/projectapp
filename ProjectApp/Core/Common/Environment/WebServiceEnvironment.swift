//
//  WebServiceEnvironment.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

protocol WebServiceEnvironment {
    var baseUrl: URL { get }
    func generateUnencodeUrl(endpoint: String) -> URL?
}

extension WebServiceEnvironment {
    func generateUnencodeUrl(endpoint: String) -> URL? {
        guard let url = URL(string: baseUrl.absoluteString + endpoint) else { return nil }
        return url
    }
}
