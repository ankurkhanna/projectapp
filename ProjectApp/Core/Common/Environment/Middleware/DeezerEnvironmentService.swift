//
//  MiddlewareEnvironmentService.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
// swiftlint:disable all
struct DeezerEnvironmentService: EnvironmentService {
    typealias ServiceType = MiddlewareConfig
    var staging: MiddlewareConfig {
        return MiddlewareConfig(environment: MiddlewareStagingEnvironment())
    }

    var production: MiddlewareConfig {
        return MiddlewareConfig(environment: MiddlewareProductionEnvironment())
    }
}

struct MiddlewareStagingEnvironment: WebServiceEnvironment {
    var baseUrl = URL(string: "\(EnvironmentBase.MiddlewareStagingBaseUrl)")!
}

struct MiddlewareProductionEnvironment: WebServiceEnvironment {
    var baseUrl = URL(string: "\(EnvironmentBase.MiddlewareProductionBaseUrl)")!
}

struct MiddlewareConfig {
    let environment: WebServiceEnvironment
}
