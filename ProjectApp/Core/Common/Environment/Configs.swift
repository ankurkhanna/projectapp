//
//  Configs.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

struct Configs {

    #if STAGING
    private static var appEnvironment: EnvironmentType = EnvironmentType.staging
    #else
    private static var appEnvironment: EnvironmentType = EnvironmentType.production
    #endif

    static var environment: EnvironmentType {
        return appEnvironment
    }
}
