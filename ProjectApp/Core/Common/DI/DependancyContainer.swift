//
//  DependancyContainer.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import Swinject

class Dependancy {
    static let container: Container = {
        var container: Container = Container()
        container = Dependancy.registerDefaultServices(inContainer: container)
        container = Dependancy.registerProviders(inContainer: container)
        container = Dependancy.registerRepositories(inContainer: container)
        return container
    }()
    
    private static func registerDefaultServices(inContainer container: Container) -> Container {
        container.register(NetworkClient.self) { r -> NetworkClient in
            return DefaultNetworkClient()
        }.inObjectScope(.container)

        container.register(Environment.self) { (_) -> Environment in
            switch Configs.environment {
            case .staging:
                return StagingEnvironment()
            case .production:
                return ProductionEnvironment()
            }
        }
        return container;
    }
    
    private static func registerProviders(inContainer container: Container) -> Container {
        container.register(DeezerMusicProvider.self) { r -> DeezerMusicProvider in
            let environment = r.resolve(Environment.self)!
            let networkClient = r.resolve(NetworkClient.self)!
            return DefaultDeezerMusicProvider(environment: environment,
                                              networkClient: networkClient)
        }
        .inObjectScope(.container)
        return container;
    }
        
    private static func registerRepositories(inContainer container: Container) -> Container {
        container.register(MusicRepository.self) { r -> MusicRepository in
            let deezerMusicProvider = r.resolve(DeezerMusicProvider.self)!
            return DefaultMusicRepository(deezerMusicProvider: deezerMusicProvider)
        }
        .inObjectScope(.container)
        return container;
    }
}
