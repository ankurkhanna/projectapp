//
//  DefaultNetworkClient.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

final class DefaultNetworkClient: NetworkClient {
    private let sessionManager: Session
 
    init() {
        sessionManager = {
          let configuration = URLSessionConfiguration.af.default
          return Session(configuration: configuration)
        }()
    }
    
    func request<T: Decodable>(urlRequest: URLRequest) -> Observable<T> {
        let result = Observable<T>.create { [weak self] observer -> Disposable in
            guard let self = self else { return Disposables.create() }
            let request = self.sessionManager.request(urlRequest)
            request
                .validate()
                .responseDecodable(of: T.self, completionHandler: { response in
                if let error = response.error?.underlyingError {
                    observer.onError(error)
                }
                if let value = response.value {
                    observer.onNext(value)
                }
                observer.on(.completed)
            })
            return Disposables.create(with: {
                request.cancel()
            })
        }
        return result
    }
}
