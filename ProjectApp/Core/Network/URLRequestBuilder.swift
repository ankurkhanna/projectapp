//
//  URLRequestBuilder.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 23/06/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum RequestParams {
    case body(_: [String: Any]?)
    case url(_: [String: String]?)
}

protocol APIRequest {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: RequestParams { get }
    var headers: [String: Any]? { get }
}

class URLRequestBuilder {
    var baseURL: URL
    var path: String
    var method: HTTPMethod = .get
    var headers: [String: Any]?
    var parameters: RequestParams?

    init(with baseURL: URL, path: String) {
        self.baseURL = baseURL
        self.path = path
    }

    @discardableResult
    func set(method: HTTPMethod) -> Self {
        self.method = method
        return self
    }

    @discardableResult
    func set(path: String) -> Self {
        self.path = path
        return self
    }

    @discardableResult
    func set(headers: [String: Any]?) -> Self {
        self.headers = headers
        return self
    }

    @discardableResult
    func set(parameters: RequestParams?) -> Self {
        self.parameters = parameters
        return self
    }

    func build() -> URLRequest {
            let url = baseURL.appendingPathComponent(path)
            var urlRequest = URLRequest(url: url)

            if let params = parameters {
                switch params {
                case .body(let bodyParams):
                    urlRequest = buildBodyParam(urlRequest, params: bodyParams)
                case .url(let urlParams):
                    urlRequest.url = buildUrlParam(url, params: urlParams)
                }
            }
            
            urlRequest.httpMethod = method.rawValue

            headers?.forEach {
                urlRequest.addValue($0.value as! String, forHTTPHeaderField: $0.key)
            }
            return urlRequest
    }
        
    fileprivate func buildBodyParam(_ urlRequest: URLRequest, params: [String: Any]?) -> URLRequest {
        var urlRequest = urlRequest
        if let params = params,
           !params.isEmpty,
           JSONSerialization.isValidJSONObject(params) == true,
           let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) {
            urlRequest.httpBody = data
        }
        return urlRequest
    }
    
    fileprivate func buildUrlParam(_ url: URL, params: [String: String]?) -> URL {
        var url = url
        if let params = params, !params.isEmpty {
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
            components?.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value) }
            url = components?.url ?? url
        }
        return url
    }
}

