//
//  NetworkService.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation
import RxSwift

protocol NetworkClient {
    func request<T: Decodable>(urlRequest: URLRequest) -> Observable<T>
}


