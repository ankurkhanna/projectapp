//
//  Chart.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import Foundation

struct Editorial: Decodable {
    let editorialItems : [EditorailItem]
    let total : Int?

    enum CodingKeys: String, CodingKey {
        case editorialItems = "data"
        case total = "total"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        editorialItems = try values.decodeIfPresent([EditorailItem].self, forKey: .editorialItems).or([])
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }
}

struct EditorailItem : Codable {
    let id : Int?
    let name : String?
    let picture : String?
    let picture_small : String?
    let picture_medium : String?
    let picture_big : String?
    let picture_xl : String?
    let type : String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case picture = "picture"
        case picture_small = "picture_small"
        case picture_medium = "picture_medium"
        case picture_big = "picture_big"
        case picture_xl = "picture_xl"
        case type = "type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        picture = try values.decodeIfPresent(String.self, forKey: .picture)
        picture_small = try values.decodeIfPresent(String.self, forKey: .picture_small)
        picture_medium = try values.decodeIfPresent(String.self, forKey: .picture_medium)
        picture_big = try values.decodeIfPresent(String.self, forKey: .picture_big)
        picture_xl = try values.decodeIfPresent(String.self, forKey: .picture_xl)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
}
