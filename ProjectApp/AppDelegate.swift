//
//  AppDelegate.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var rootCoordinator: RootCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        self.rootCoordinator = RootCoordinator(window: window)
        _ = rootCoordinator?.start()
        return true
    }
}
