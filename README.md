# README #

### What is this repository for? ###

* The ProjectApp uses Deezer API (editorial API) to showcase a list editorials in a table view list. The project focuses on the architetural pattern and code structure keeping the UI pretty straightforward.

### How do I set up? ###

* Cocoapods have been committed for the use of access, so can clone/ download repository and run the app.
* Xcode version : 12.5
* Cocoapods version 1.10.1

### Dependancy ###
* Using Cocoapods eg: RxSwift, RxDataSources, Alamofire, Swinject, SwiftLint etc.

### Project Struture ###
* Architectural Pattern: MVVM-C alongwith Rxswift
* The project has been bifurcated using scheme and environment to showcase a separation of Prodction + Staging App.

* Core : It contains all the services, repositories, provider, network layer, api, model, dependency injection and enviornment used to get the api data.
* Presentation : It include root structure, base classes and initial(landing) screen along wih table cell to showcase the list of contents.

### How to run tests ###
* Navigating to test bundle and clicking the arrow icon to run all test. It covers business logic for ViewModel, repository, provider and model.