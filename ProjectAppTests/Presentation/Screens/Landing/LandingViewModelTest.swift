//
//  LandingViewModelTest.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick
import Mockingjay
import RxBlocking

@testable import ProjectApp
class LandingViewModelTest: QuickSpec {
    
    private func setupSuccessStub() -> Stub {
        let path = Bundle(for: type(of: self)).path(forResource: "GetEditorial", ofType: "json")!
        let data = NSData(contentsOfFile: path)!
        return stub(uri("https://api.deezer.com/editorial"), jsonData(data as Data))
    }
    
    private func setupFailureStub() -> Stub {
        return stub(uri("https://api.deezer.com/editorial"), http(404))
    }

    override func spec() {
        var vm: LandingViewModel!
        var viewData: LandingViewModel.ViewData!
        var viewEvent: LandingViewModel.ViewEvent!
        var disposable: Disposable!
        let startScreenSubject: PublishSubject<Void> = PublishSubject<Void>()
        let musicRepository: MusicRepository = Dependancy.container.resolve(MusicRepository.self)!
        var mockApiStub: Stub?

        beforeEach {
            vm = LandingViewModel(musicRepository: musicRepository)
            viewEvent = LandingViewModel.ViewEvent(startScreen: startScreenSubject.asObservable())
            disposable = vm.set(viewEvent: viewEvent)
            viewData = vm.generateViewData()
            guard let mockApiStub = mockApiStub else { return }
            self.removeStub(mockApiStub)
        }

        afterEach {
            disposable.dispose()
        }
        
        describe("ViewModel Test") {
            it("Should return n number of sections") {
                mockApiStub = self.setupSuccessStub()
                startScreenSubject.onNext(())
                let sections = try! viewData.sections.skip(1).toBlocking().first()!
                expect(sections.count).toNot(equal(0))
            }
            
            it("Should fail and return 0 results") {
                mockApiStub = self.setupFailureStub()
                startScreenSubject.onNext(())
                let sections = try! viewData.sections.skip(1).toBlocking().first()!
                expect(sections.count).to(equal(0))
            }
            
            it("Should return 1 sections with 22 items") {
                mockApiStub = self.setupSuccessStub()
                startScreenSubject.onNext(())
                let sections: [EditorialListingSection] = try! viewData.sections.skip(1).toBlocking().first()!
                print(sections)
                expect(sections.count).to(equal(1))
                expect(sections.first).toNot(beNil())
                expect(sections.first?.items).toNot(beNil())
                expect(sections.first?.items.count).to(equal(22))
                expect(sections.first?.items.first).toNot(beNil())
            }
            
        }
    }
}
