//
//  RootViewModelTest.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick
import Mockingjay
import RxBlocking

@testable import ProjectApp
class RootViewScreenTest: QuickSpec {
    
    override func spec() {
        describe("RootViewModel Test") {
            it("Should generate viewData as expected") {
                let vm: RootViewModel = RootViewModel()
                let viewEvent: RootViewModel.ViewEvent = RootViewModel.ViewEvent()
                let _ = vm.set(viewEvent: viewEvent)
                let viewData: RootViewModel.ViewData = vm.generateViewData()
                expect(viewData).toNot(beNil())
            }
            
            it("Should have output observable") {
                let vm: RootViewModel = RootViewModel()
                expect(vm.output).toNot(beNil())
            }
        }
        
        describe("RootViewController ViewEvent") {
            it("Test RootViewController viewEvent") {
                expect {
                    let vc: RootViewController = RootViewController()
                    vc.viewModel = RootViewModel()
                    vc.setViewModelViewEvent()
                    vc.bindViewData()
                    vc.setupUI()
                }.toNot(throwAssertion())
            }
        }
    }
}
