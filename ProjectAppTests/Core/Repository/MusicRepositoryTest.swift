//
//  MusicRepositoryTest.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick
import RxTest
import Mockingjay

@testable import ProjectApp

class MusicRepositoryTest: QuickSpec {
    
    private func setupSuccessStub() -> Stub {
        let path = Bundle(for: type(of: self)).path(forResource: "GetEditorial", ofType: "json")!
        let data = NSData(contentsOfFile: path)!
        return stub(uri("https://api.deezer.com/editorial"), jsonData(data as Data))
    }
    
    private func setupFailureStub() -> Stub {
        return stub(uri("https://api.deezer.com/editorial"), http(404))
    }

    override func spec() {
        let musicProvider: DeezerMusicProvider = Dependancy.container.resolve(DeezerMusicProvider.self)!
        var mockApiStub: Stub?

        beforeEach {
            guard let mockApiStub = mockApiStub else { return }
            self.removeStub(mockApiStub)
        }

        afterEach {
        }
        
        describe("Music Repository Test") {
            it("Test Get return 22 Editorials List") {
                mockApiStub = self.setupSuccessStub()
                let musicRepo: MusicRepository = DefaultMusicRepository(deezerMusicProvider: musicProvider)
                let editorial: Editorial = try! musicRepo.getEditoials().toBlocking().first()!
                expect(editorial).toNot(beNil())
                expect(editorial.total).to(equal(22))
                expect(editorial.editorialItems.count).to(equal(22))
                expect(editorial.editorialItems.first).toNot(beNil())
                expect(editorial.editorialItems.first!.name).to(equal("All"))
                expect(editorial.editorialItems.first!.picture).to(equal("http://cdn-images.dzcdn.net/images/misc//180x180-000000-80-0-0.jpg"))
            }
            
            it("Test Get return 0 Editorials List") {
                mockApiStub = self.setupFailureStub()
                let musicRepo: MusicRepository = DefaultMusicRepository(deezerMusicProvider: musicProvider)
                let editorial: Editorial? = try? musicRepo.getEditoials().toBlocking().first()
                expect(editorial).to(beNil())
            }
        }
    }
}
