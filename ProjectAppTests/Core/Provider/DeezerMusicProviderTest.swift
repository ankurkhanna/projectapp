//
//  DeezerMusicProviderTest.swift
//  ProjectApp
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick
import RxTest
import Mockingjay

@testable import ProjectApp

class DeezerMusicProviderTest: QuickSpec {
    
    private func setupSuccessStub() -> Stub {
        let path = Bundle(for: type(of: self)).path(forResource: "GetEditorial", ofType: "json")!
        let data = NSData(contentsOfFile: path)!
        return stub(uri("https://api.deezer.com/editorial"), jsonData(data as Data))
    }
    
    private func setupFailureStub() -> Stub {
        return stub(uri("https://api.deezer.com/editorial"), http(404))
    }

    override func spec() {
        
        var mockApiStub: Stub?
        let getEditorailsAPI: GetEditorialAPI = Dependancy.container.resolve(GetEditorialAPI.self)!
        
        beforeEach {
            guard let mockApiStub = mockApiStub else { return }
            self.removeStub(mockApiStub)
        }

        afterEach {
        }
        
        describe("Music Provider Test") {
            it("Test Get return 22 Editorials List") {
                mockApiStub = self.setupSuccessStub()
                let musicProvider: DeezerMusicProvider = DefaultDeezerMusicProvider(getEditorailsAPI: getEditorailsAPI)
                let editorial: Editorial = try! musicProvider.getEditoials().toBlocking().first()!
                expect(editorial).toNot(beNil())
                expect(editorial.total).to(equal(22))
                expect(editorial.editorialItems.count).to(equal(22))
                expect(editorial.editorialItems.first).toNot(beNil())
                expect(editorial.editorialItems.first!.name).to(equal("All"))
                expect(editorial.editorialItems.first!.picture).to(equal("http://cdn-images.dzcdn.net/images/misc//180x180-000000-80-0-0.jpg"))
            }
            
            it("Test Get return 0 Editorials List") {
                mockApiStub = self.setupFailureStub()
                let musicProvider: DeezerMusicProvider = DefaultDeezerMusicProvider(getEditorailsAPI: getEditorailsAPI)
                let editorial: Editorial? = try? musicProvider.getEditoials().toBlocking().first()
                expect(editorial).to(beNil())
            }
        }
        
        describe("Call Deezer API") {
            it("Execute Deezer HTTP Call") {
                mockApiStub = self.setupSuccessStub()
                let getEditorialAPI: GetEditorialAPI = Dependancy.container.resolve(GetEditorialAPI.self)!
                let responseObservable: Observable<Void> = getEditorialAPI.response(reqPayload: GetEditorialAPI.RequestPayload())
                let response: Void = try! responseObservable.toBlocking().first()!
                expect(response).to(beVoid())
            }
            
            it("Handle Error Deezer HTTP Call") {
                mockApiStub = self.setupFailureStub()
                let getEditorialAPI: GetEditorialAPI = Dependancy.container.resolve(GetEditorialAPI.self)!
                let responseObservable: Observable<Void> = getEditorialAPI.response(reqPayload: GetEditorialAPI.RequestPayload())
                expect {
                    let _ = try! responseObservable.toBlocking().first()!
                }.to(throwAssertion())
                
            }
        }
    }
}
