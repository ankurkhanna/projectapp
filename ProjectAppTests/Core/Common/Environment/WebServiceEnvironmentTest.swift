//
//  WebServiceEnvironmentTest.swift
//  ProjectAppTests
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick
import RxTest

@testable import ProjectApp

class WebServiceEnvironmentTest: QuickSpec {
    
    override func spec() {

        beforeEach {

        }

        afterEach {
        }
        
        describe("WebServiceEnvironment Test") {
            it("Test Get generateUnencodeUrl") {
                let webServiceEnvStaging: WebServiceEnvironment = MiddlewareStagingEnvironment()
                let webServiceEnvProduction: WebServiceEnvironment = MiddlewareProductionEnvironment()
                expect(webServiceEnvStaging.baseUrl.absoluteString).to(equal("https://api.deezer.com"))
                expect(webServiceEnvProduction.baseUrl.absoluteString).to(equal("https://api.deezer.com"))
                expect(webServiceEnvStaging.generateUnencodeUrl(endpoint: "/editorial")).toNot(beNil())
                expect(webServiceEnvStaging.generateUnencodeUrl(endpoint: "/editorial")!.absoluteString).to(equal("https://api.deezer.com/editorial"))
                expect(webServiceEnvProduction.generateUnencodeUrl(endpoint: "/editorial")!.absoluteString).toNot(beNil())
                expect(webServiceEnvProduction.generateUnencodeUrl(endpoint: "/editorial")!.absoluteString).to(equal("https://api.deezer.com/editorial"))
            }
            
            it("Test Fail to generateUnencodeUrl") {
                let webServiceEnvStaging: WebServiceEnvironment = MiddlewareStagingEnvironment()
                let webServiceEnvProduction: WebServiceEnvironment = MiddlewareProductionEnvironment()
                expect(webServiceEnvStaging.generateUnencodeUrl(endpoint: "||")).to(beNil())
                expect(webServiceEnvProduction.generateUnencodeUrl(endpoint: "||")).to(beNil())
            }
        }
        
        describe("Environment Test") {
            it("Test Envs") {
                let stagingEnv: Environment = StagingEnvironment()
                expect(stagingEnv.type).to(equal(.staging))
                let prodEnv: Environment = ProductionEnvironment()
                expect(prodEnv.type).to(equal(.production))
            }
        }
        
        describe("Deezer Environment Service") {
            it("Test if Staging Middleware Config exists") {
                let envService: DeezerEnvironmentService = DeezerEnvironmentService()
                expect(envService.staging.environment.baseUrl.absoluteString).to(equal("https://api.deezer.com"))
                expect(envService.production.environment.baseUrl.absoluteString).to(equal("https://api.deezer.com"))
            }
        }
    }
}
