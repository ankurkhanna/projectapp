//
//  UtilTest.swift
//  ProjectAppTests
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//



import XCTest
import RxSwift
import Nimble
import Quick
import RxTest

@testable import ProjectApp

class UtilTest: QuickSpec {
    
    override func spec() {

        beforeEach {

        }

        afterEach {
        }
        
        describe("Dictionary Test") {
            it("Test toUrlQueryItems") {
                let dictionary: Dictionary<String, String> = ["Hello": "bello"]
                let queryItems: [URLQueryItem] = dictionary.toURLQueryItems()
                expect(queryItems.count).to(equal(1))
                expect(queryItems.first!.name).to(equal("Hello"))
                expect(queryItems.first!.value).to(equal("bello"))
            }
        }
        
        describe("Optional Chain Test") {
            it("Optional or Test") {
                let item1: String? = nil
                let item2: String? = "hi"
                expect(item1.or(item2)).to(equal("hi"))
            }
        }
        
        describe("TableViewCell dispose Test") {
            it("Test disposeOnReuse") {
                let cell: TableViewCell = TableViewCell(frame: CGRect.zero)
                cell.insertSubview(TableViewCell(frame: CGRect.zero), at: 0)
                cell.prepareForReuse()
                expect(cell.disposeBag).toNot(beNil())
            }
        }
        
        describe("URL extension Test") {
            it("URL extendUrl") {
                let url: URL = URL(string: "https://google.com")!
                let newUrl: URL = url.extendUrl(withPath: "/search", queryParams: ["Hello": "bello"])
                expect(newUrl).toNot(beNil())
                expect(newUrl.absoluteString).to(equal("https://google.com/search?Hello=bello"))
            }
        }

        
        describe("NetworkClient Test") {
            it("Network Client Void Request") {
                let networkClient: NetworkClient = Dependancy.container.resolve(NetworkClient.self)!
                let reqObservable: Observable<Void> = networkClient.request(url: URL(string: "https://google.com")!, method: .get, headers: [: ], body: NetworkBody())
                let response: Void = try! reqObservable.toBlocking().first()!
                expect(response).to(beVoid())
            }
            
            it("Network Client Void Request Empty Data") {
                let networkClient: NetworkClient = Dependancy.container.resolve(NetworkClient.self)!
                let reqObservable: Observable<Void> = networkClient.request(url: URL(string: "https://google.com")!, method: .get, headers: [: ], body: NetworkBody(data: "hi"))
                let response: Void = try! reqObservable.toBlocking().first()!
                expect(response).to(beVoid())
            }
        }

    }
}

