//
//  ViewControllerTest.swift
//  ProjectAppTests
//
//  Created by Ankur Khanna on 24/05/2021.
//  Copyright © 2021 Ankur Khanna. All rights reserved.
//

import XCTest
import RxSwift
import Nimble
import Quick
import Mockingjay
import RxBlocking

@testable import ProjectApp
class ViewControllerTest: QuickSpec {
    
    override func spec() {
        
        describe("ViewController Override Functions Test") {
            it("BaseViewController setViewModelViewEvent Test") {
                expect {
                    let vc: BaseViewController = BaseViewController()
                    vc.setViewModelViewEvent()
                }.to(throwAssertion())
            }
            
            it("BaseViewController bindViewData Test") {
                expect {
                    let vc: BaseViewController = BaseViewController()
                    vc.bindViewData()
                }.to(throwAssertion())
            }
            
            it("BaseViewController setupUI Test") {
                expect {
                    let vc: BaseViewController = BaseViewController()
                    vc.setupUI()
                }.to(throwAssertion())
            }
        }
    }
}
